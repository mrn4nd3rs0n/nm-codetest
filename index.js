import * as fmpService from "./services/financialModelingPrep.js";
import * as portfolioService from "./services/portfolio.js";
import * as createCsvWriter from "csv-writer";
import prompts from "prompts";
import moment from "moment";

const savePortfolioCSV = async ({
  symbols,
  shareQuantities = [5, 10, 15],
  fileName,
}) => {
  //get historical stock price from financialmodelingprep.com
  const historicalPriceProfile = await fmpService.getBatchHistoricalPriceByDateRange(
    { symbols }
  );

  // reduce historical prices down to portfolio entries
  let portfolioValues = historicalPriceProfile.historicalStockList.map(
    (historicalStock, index) =>
      portfolioService.getStockValueFromHistoricalPriceList({
        shares: shareQuantities[index],
        symbol: historicalStock.symbol,
        historicalPriceList: historicalStock.historical,
      })
  );

  // create portfolio totals entry
  let portfolioTotal = {
    ticker: "Total",
    quantity: "",
    currentPrice: "",
    high: "",
    low: "",
    currentValue: portfolioValues.reduce((sum, value) => {
      return value.currentValue + sum;
    }, 0),
  };

  //convert portfolio values to currency format
  portfolioValues = portfolioValues.map((portfolioValue) =>
    portfolioService.changePortfolioValueToCurrency(portfolioValue)
  );
  //convert portfolio totals to currency format
  portfolioTotal = portfolioService.changePortfolioValueToCurrency(
    portfolioTotal
  );

  //format csv
  const csvWriter = createCsvWriter.createObjectCsvWriter({
    path: fileName
      ? fileName + ".csv"
      : "stocks-" + moment().format("YYYYMMDD") + ".csv",
    header: [
      { id: "ticker", title: "Ticker" },
      { id: "quantity", title: "Quantity" },
      { id: "currentPrice", title: "Current Price" },
      { id: "high", title: "High" },
      { id: "low", title: "Low" },
      { id: "currentValue", title: "Current Value" },
    ],
  });
  let records = [...portfolioValues];
  records.push(portfolioTotal);

  //write csv
  await csvWriter.writeRecords(records);
};

//prompt user for input
(async () => {
  try {
    const questions = [
      {
        type: "text",
        name: "symbols",
        message:
          "Please enter a comma delimited list of stock symbols. Min 1, Max 3",
      },
      {
        type: "text",
        name: "quantities",
        message:
          "Please enter a comma delimited list of number share quantities for each stock.",
      },
      {
        type: "text",
        name: "fileName",
        message: "Please enter filename",
      },
    ];
    console.log("Welcome to the stock value generator.");
    const promptResponse = await prompts(questions);
    let symbols = promptResponse.symbols.toUpperCase();
    const symbolsRegex = /^[A-Z,\-,\\,]+$/;
    const quantitiesRegex = /^[0-9,\\,]+$/;
    const fileNameRegex = /^[A-Z,0-9]+$/i;
    const symbolArray = symbols.split(",");
    const quantitiesArray = promptResponse.quantities.split(",");
    //validate user input
    if (
      !symbolsRegex.test(symbols) ||
      !fileNameRegex.test(promptResponse.fileName) ||
      !quantitiesRegex.test(promptResponse.quantities) ||
      symbolArray.length > 3 ||
      symbolArray.length !== quantitiesArray.length
    ) {
      console.log("invalid input using defaults");
      await savePortfolioCSV({});
      console.log("stocks-" + moment().format("YYYYMMDD") + ".csv generated");
    } else {
      await savePortfolioCSV({
        symbols: symbolArray,
        fileName: promptResponse.fileName,
        shareQuantities: quantitiesArray,
      });
      console.log(promptResponse.fileName + ".csv generated");
    }
  } catch (err) {
    console.log(err);
  }
})();
