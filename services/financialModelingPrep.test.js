import * as fmpService from "./financialModelingPrep.js";

describe("financialModelingPrep.getBatchHistoricalPriceByDateRange tests", () => {
  test("it throws symbols required", async () => {
    try {
      await fmpService.getBatchHistoricalPriceByDateRange({
        symbols: null,
      });
    } catch (err) {
      expect(err.message).toMatch("symbols required");
    }
  });
  test("it throws symbols must be array", async () => {
    try {
      await fmpService.getBatchHistoricalPriceByDateRange({
        symbols: "test",
      });
    } catch (err) {
      expect(err.message).toMatch("symbols must be array");
    }
  });
  test("it throws symbols length must be between 1 and 3 (0 values)", async () => {
    try {
      await fmpService.getBatchHistoricalPriceByDateRange({
        symbols: [],
      });
    } catch (err) {
      expect(err.message).toMatch("symbols length must be between 1 and 3");
    }
  });
  test("it throws symbols length must be between 1 and 3 (4 values)", async () => {
    try {
      await fmpService.getBatchHistoricalPriceByDateRange({
        symbols: ["A", "A", "A", "A"],
      });
    } catch (err) {
      expect(err.message).toMatch("symbols length must be between 1 and 3");
    }
  });
});
