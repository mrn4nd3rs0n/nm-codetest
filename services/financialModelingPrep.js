import fetch from "node-fetch";
import moment from "moment";

const apiBase = "https://financialmodelingprep.com";

export const getCompanyProfile = async ({ symbol }) => {
  const CompanyProfileResult = await fetch(
    apiBase + "/api/v3/company/profile/" + symbol
  );
  const CompanyProfileJson = await CompanyProfileResult.json();
  return CompanyProfileJson;
};

export const getBatchHistoricalPriceByDateRange = async ({
  symbols = ["AAPL,GOOG,FB"],
  startDate = "2019-01-01",
  endDate = moment().format("YYYY-MM-DD"),
}) => {
  //validate inputs
  if (!symbols) throw new Error("symbols required");
  if (!Array.isArray(symbols)) throw new Error("symbols must be array");
  if (symbols.length < 1 || symbols.length > 3)
    throw new Error("symbols length must be between 1 and 3");
  const batchHistoricalPriceByDateRangeResult = await fetch(
    apiBase +
      "/api/v3/historical-price-full/" +
      symbols.toString() +
      "?from=" +
      startDate +
      "&to=" +
      endDate
  );
  const batchHistoricalPriceByDateRangeJson = await batchHistoricalPriceByDateRangeResult.json();
  return batchHistoricalPriceByDateRangeJson;
};
