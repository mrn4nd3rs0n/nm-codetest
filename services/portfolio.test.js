import * as portfolioService from "./portfolio.js";

describe("portfolioService.getStockValueFromHistoricalPriceList tests", () => {
  test("it throws shares cannot be negative", async () => {
    try {
      await portfolioService.getStockValueFromHistoricalPriceList({
        shares: -1,
        symbol: "TEST",
        historicalPriceList: [],
      });
    } catch (err) {
      expect(err.message).toMatch("shares cannot be negative");
    }
  });
  test("it throws symbol required", async () => {
    try {
      await portfolioService.getStockValueFromHistoricalPriceList({
        shares: 5,
        symbol: "",
        historicalPriceList: [],
      });
    } catch (err) {
      expect(err.message).toMatch("symbol required");
    }
  });
  test("it throws historicalPriceList required", async () => {
    try {
      await portfolioService.getStockValueFromHistoricalPriceList({
        shares: 5,
        symbol: "TEST",
        historicalPriceList: null,
      });
    } catch (err) {
      expect(err.message).toMatch("historicalPriceList required");
    }
  });
  test("it throws historicalPriceList must contain at least one value", async () => {
    try {
      await portfolioService.getStockValueFromHistoricalPriceList({
        shares: 5,
        symbol: "TEST",
        historicalPriceList: [],
      });
    } catch (err) {
      expect(err.message).toMatch(
        "historicalPriceList must contain at least one value"
      );
    }
  });
  test("it returns stockValue successfully", async () => {
    const historicalPriceList = [
      {
        date: "2018-03-12",
        open: 180.29,
        high: 182.39,
        low: 180.21,
        close: 181.72,
        adjClose: 177.63,
        volume: 3.22071e7,
        unadjustedVolume: 3.22071e7,
        change: 1.43,
        changePercent: 0.793,
        vwap: 181.44,
        label: "March 12, 18",
        changeOverTime: 0.00793,
      },
      {
        date: "2018-03-13",
        open: 182.59,
        high: 183.5,
        low: 179.24,
        close: 179.97,
        adjClose: 175.92,
        volume: 3.16935e7,
        unadjustedVolume: 3.16935e7,
        change: -2.62,
        changePercent: -1.435,
        vwap: 180.90333,
        label: "March 13, 18",
        changeOverTime: -0.01435,
      },
      {
        date: "2018-03-14",
        open: 180.32,
        high: 180.52,
        low: 177.81,
        close: 178.44,
        adjClose: 174.43,
        volume: 2.93684e7,
        unadjustedVolume: 2.93684e7,
        change: -1.88,
        changePercent: -1.043,
        vwap: 178.92333,
        label: "March 14, 18",
        changeOverTime: -0.01043,
      },
    ];

    const stockValue = await portfolioService.getStockValueFromHistoricalPriceList(
      {
        shares: 5,
        symbol: "AAPL",
        historicalPriceList,
      }
    );
    const expectedValue = {
      ticker: "AAPL",
      quantity: 5,
      currentPrice: 178.44,
      high: 181.72,
      low: 178.44,
      currentValue: 892.2,
    };
    expect(stockValue).toEqual(expectedValue);
  });
});
describe("portfolioService.changePortfolioValueToCurrency tests", () => {
  test("it throws currentPrice must be a number", async () => {
    try {
      await portfolioService.changePortfolioValueToCurrency({
        ticker: "",
        quantity: 2,
        currentPrice: "100",
        high: 100,
        low: 50.555,
        currentValue: 200,
      });
    } catch (err) {
      expect(err.message).toMatch("currentPrice must be a number");
    }
  });
  test("it throws high must be a number", async () => {
    try {
      await portfolioService.changePortfolioValueToCurrency({
        ticker: "",
        quantity: 2,
        currentPrice: 100,
        high: "100",
        low: 50.555,
        currentValue: 200,
      });
    } catch (err) {
      expect(err.message).toMatch("high must be a number");
    }
  });
  test("it throws low must be a number", async () => {
    try {
      await portfolioService.changePortfolioValueToCurrency({
        ticker: "",
        quantity: 2,
        currentPrice: 100,
        high: 100,
        low: "50.555",
        currentValue: 200,
      });
    } catch (err) {
      expect(err.message).toMatch("low must be a number");
    }
  });
  test("it throws currentValue must be a number", async () => {
    try {
      await portfolioService.changePortfolioValueToCurrency({
        ticker: "",
        quantity: 2,
        currentPrice: 100,
        high: 100,
        low: 50.555,
        currentValue: "200",
      });
    } catch (err) {
      expect(err.message).toMatch("currentValue must be a number");
    }
  });
  test("it returns portfolioValueToCurrency successfully", async () => {
    const portfolioToCurrencyValue = await portfolioService.changePortfolioValueToCurrency(
      {
        ticker: "TEST",
        quantity: 2,
        currentPrice: 100,
        high: 100,
        low: 50.555,
        currentValue: 200,
      }
    );
    const expectedValue = {
      ticker: "TEST",
      quantity: 2,
      currentPrice: "$100.00",
      high: "$100.00",
      low: "$50.56",
      currentValue: "$200.00",
    };
    expect(portfolioToCurrencyValue).toEqual(expectedValue);
  });
});
