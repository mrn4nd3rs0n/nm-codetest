import roundTo from "round-to";

export const getStockValueFromHistoricalPriceList = ({
  shares,
  symbol,
  historicalPriceList,
}) => {
  //validate inputs
  if (shares < 0) throw new Error("shares cannot be negative");
  if (!symbol) throw new Error("symbol required");
  if (!historicalPriceList) throw new Error("historicalPriceList required");
  if (!historicalPriceList.length || historicalPriceList.length === 0)
    throw new Error("historicalPriceList must contain at least one value");
  //date sorted collection
  const dateSortedPrices = [...historicalPriceList].sort(
    (a, b) => a.date - b.date
  );
  //closing price sorted collection
  const closeValueSortedPrices = [...historicalPriceList].sort(
    (a, b) => a.close - b.close
  );
  const currentPrice = dateSortedPrices[dateSortedPrices.length - 1].close;
  const high = closeValueSortedPrices[closeValueSortedPrices.length - 1].close;
  const low = closeValueSortedPrices[0].close;

  const currentValue = currentPrice * shares;
  return {
    ticker: symbol,
    quantity: shares,
    currentPrice,
    high,
    low,
    currentValue,
  };
};

export const changePortfolioValueToCurrency = ({
  ticker,
  quantity,
  currentPrice,
  high,
  low,
  currentValue,
}) => {
  if (currentPrice && typeof currentPrice !== "number")
    throw new Error("currentPrice must be a number");
  if (high && typeof high !== "number")
    throw new Error("high must be a number");
  if (low && typeof low !== "number") throw new Error("low must be a number");
  if (currentValue && typeof currentValue !== "number")
    throw new Error("currentValue must be a number");

  return {
    ticker: ticker ? ticker : "",
    quantity: quantity ? quantity : "",
    currentPrice: currentPrice ? "$" + roundTo(currentPrice, 2).toFixed(2) : "",
    high: high ? "$" + roundTo(high, 2).toFixed(2) : "",
    low: low ? "$" + roundTo(low, 2).toFixed(2) : "",
    currentValue: currentValue ? "$" + roundTo(currentValue, 2).toFixed(2) : "",
  };
};
