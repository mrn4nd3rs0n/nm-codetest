# nm-codetest

## install node.js 12.16.2 LTS

https://nodejs.org/en/

## Clone repository

```
git clone https://gitlab.com/mrn4nd3rs0n/nm-codetest.git
```

## Navigate into directory nm-codetest

```
cd nm-codetest
```

## Install dependencies

```
npm install
```

## Generate stock portfolio file

```
npm run start
```

## Run unit tests

```
npm run test
```
